# Copyright (c) 2021 Helmholtz-Zentrum Dresden - Rossendorf e.V.
#
# SPDX-License-Identifier: MIT

add_executable(helloWorld main.cpp helloworld.cpp)
